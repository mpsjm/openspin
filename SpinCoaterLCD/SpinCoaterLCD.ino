// Sketch to control spin coater - builds on the SpinCoaterBasic sketch
// Adds measurement of rpm
//
// 18/2/2012 modify LCD code to give constant width output
//
// 3/2/2012 Add LCD support - requires some changing of pin numbers LCD taked d8-d13


// note sending time signals to ESC

// 1000 -> minimum setting
// 2000-> maximum setting
// 1106 appears to be the minimum timing signal that the motor will turn for


// esc is attached to pin d5
// digital pin used to activate the motor = d6
// analog pin used to connect the potentiometer= a2
// speed read via interrupt signal to d3 (interrupt pin 1)

#include <Servo.h>      // Servo library can control ESCs
#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(8, 9, 10, 11, 12, 13);
 
Servo myESC;            // create servo object to control an ESC 
volatile byte revCount; // used by interrupt
 int rpm;       // value of rpm
// RPM is calculated by measuring time taken to accumulate at least 40 interrupt signals
unsigned long time;     // current time
unsigned long timeOld;  // previously recorded time value
//
int potpin = 2;              // analog pin used to connect the potentiometer
unsigned int val =0;         // variable to read the value from the analog pin 
const int buttonpin=6;       // digital pin used to activate the motor
int buttonState=LOW;         // state of button
int currentThrottleValue=1000; // stores value of throttle currently in use - defaults to motor off value
int ESCpause=10;             // delay in millseconds between changes to the throttle value - ramp time
int width=4;                   // number of digits used for speed
const int minThrottle=1000;

void arm()
{
  
  myESC.writeMicroseconds(1000); // arming signal (zero throttle)
  delay(2000);                   // pause
}

void setup()
{  
  myESC.attach(5);             // esc is attached to pin 5 
  lcd.begin(16, 2);            // set up the LCD's number of columns and rows
  lcd.print("rpm:");           // Print a message to the LCD. 

  arm();                       // need to arm the ESC
  pinMode(buttonpin,INPUT);    // button will be sensed via digital pin buttonpin
  attachInterrupt (1,rpmFunction,FALLING); // call rpmFunction each time the interupt is recieved.
                                           // interupt 1 is digital pin 3

}

void setThrottle()
{
//	  lcd.setCursor(15,0);
//  lcd.print("c");
  // This routime changes throttle value to requested value
  // has to change value smoothly - or the ESC will ignore it use a ramp (up and down)
 if (val> currentThrottleValue)
 {
   for (int i=currentThrottleValue;i<=val;i++)
   {
     myESC.writeMicroseconds(i);
     delay(ESCpause);            // this pause ensures that ESC accepts new value
   }
 } else if (val<currentThrottleValue)
   {
   	//Serial.println("Decreasing throttle");
     for (int i=currentThrottleValue;i>=val;i--)
     {
     myESC.writeMicroseconds(i);
     delay(ESCpause);
     }
   } else
   {
   	// same value case - can aply when restarting at same speed
   	myESC.writeMicroseconds(val);
   }
 currentThrottleValue=val; // store new value of throttle value
}
 

void loop() 
{ 
//	  lcd.setCursor(15,0);
//  lcd.print("l");
  buttonState=digitalRead(buttonpin); // check state of button
  if(buttonState==LOW)                // button is pressed
  {
//  lcd.setCursor(0,1);
//  lcd.print("button pressed");                  //set display to zero
  	
    val = analogRead(potpin);         // reads the value of the potentiometer (value between 0 and 1023) 
  
  val = map(val, 0, 1023, 1106, 1400);     // scale it to use it with the ESC timings (value between 1000 and 2000)
  // note have limited upper motor speed.
  // tests indicate that 1106 is the minimum value to get the motor going.
  if (val<1106) 
     {
        myESC.writeMicroseconds(1100); // back to zero rpm
        lcd.setCursor(5,0);
//        lcd.print(0);                  //set display to zero
        int zero=0;
        printNumber(&zero,&width);
          // Serial.println(val,DEC);
        currentThrottleValue=1100; // save new value of throttle value
      }
   else {
           setThrottle();
         } // end of if val<1106
       
  } else // button is not pressed
  {
  if (currentThrottleValue>minThrottle) // need to slow to a stop
  {
  	for (int i=currentThrottleValue;i>=minThrottle;i--)
     {
     myESC.writeMicroseconds(i);
     delay(ESCpause);
     }
     // ensure zero throttle state
    currentThrottleValue=minThrottle;        // reset value ready for next press
    lcd.setCursor(5,0);
    lcd.print("   0");                  //set display rpm to zero
    lcd.setCursor(0,1);
    lcd.print("Not active      "); 
  
  }  // endif
  
  }// end of else
//
//
// this block determines the rpm
  if(revCount>40){
  	
    //have enough counts to work out rpm
    time=micros();
    
    
    rpm=(60000000*revCount)/(time-timeOld); // counts per micro second * 1000000 *60 gives rpm.     
    
   // Serial.println(time-timeOld,DEC);
    timeOld=time; // store previous time
    revCount=0; // reset counting.
    /*
      report speed to LCD
      */
    lcd.setCursor(5, 0);                   // put cursor on second line    
//    lcd.print(rpm);                    // print out speed on LCD
    printNumber(&rpm,&width);
  }// end of if
//
// end of rpm calculation
//
}  // end of loop        

// it was found that that need to steadily increase time value to 1350 before motor starts

// will need to write routine that takes time from say 1200 to required value in short time

// further studies indicate that motor can run with 1106 as the minimum value, however the value needs to stay steady for approx 10ms for ESC to respond

void rpmFunction(){
  revCount++; //incremented on each interupt
  //Serial.println("rev");
}

void printNumber(int *value, int *width)
{
    // outputs the number *value with the number of digits given by width - adds leading blank characters if needed
    String paddedNumber=String(' ');
    int numberSize=log10(*value)+1; // gives number of digits in the number
    if (numberSize==*width)
    {
        //number is the right size
        lcd.print(*value);
    } else
    {
        // need to form a string with leading spaces
        
        for (int i=0;i<numberSize-*width-1;i++) paddedNumber=paddedNumber+' ';
        paddedNumber=paddedNumber+*value;
        lcd.print(paddedNumber);
     }
}